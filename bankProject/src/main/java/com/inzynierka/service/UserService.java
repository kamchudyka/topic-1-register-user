package com.inzynierka.service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.inzynierka.repository.RoleRepository;
import com.inzynierka.repository.UserRepository;
import com.inzynierka.core.model.User;
import com.inzynierka.core.model.security.UserRole;

@Service
@Transactional
public class UserService {
	
	private static final Logger LOG = LoggerFactory.getLogger(UserService.class);
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
    private RoleRepository roleRepository;

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;
    
    @Autowired
    private AccountService accountService;

    public User findByUsername(String username) {
        return userRepository.findByUsername(username);
    }

    private User findByEmail(String email) {
        return userRepository.findByEmail(email);
    }

    public User createUser(User user){
        //Najpierw zdefniuj role uzytkownika poprzez wywolanie metody saveUserRoles(User user);

        //Nastepnie odpytaj baze danych czy dany uzytkownik juz istnieje. Jezeli tak to: rzuc wyjatkiem, wyrzuć błąd na konsole, bądź..
        //Aby wyrzucic blad na konsole skorzystaj z Log.info() - na gorze jest zaincjalizowany pod ta potrzebe Logger

        //Jeżeli dany użytkownik nie istnieje to rozpocznij procedure tworzenia poprzez wywolanie metody buildAndSaveUser() oraz przypisz jej wynik do zmiennej localUser

        //Na koncu zwroc obiekt localUser

        return null;
    }

    private User buildAndSaveUser(User user, Set<UserRole> userRoles){
        //Zacznij od ustawienia hasła użytkownikowi. Kazdy obiekt encji w Springu ma metody inicjalizujace wartosc (settery). Podpowiedz: Haslo uzytkownika jest zakodowane
        //wiec musisz je odkodowac (uzyj passwordEncodera, ktory zostal wstrzykniety do naszego serwisu

        //Przypisz uzytkownikowi role przekazane do tej metody. Kazdy obiekt encji w Springu posiada rowniez metody pobierajace wartosc (gettery). Jezeli wartoscia jest kolekcja to
        //mozna dodac do niej nowe elementy po pobraniu jej za pomoca metody typu getter.

        //Ustaw uzytkownikowi dwa konta (primary oraz savings). Aby utworzyc te konta skorzystaj z serwisu ktory zostal wstrzykniety (AccountService)
        //serwis ten posiada dwie metody odpowiadajace za utworzenie konta

        //Na koncu zapisz uzytkownika w bazie danych. Obiekty zapisujemy/pobieramy z bazy danych za pomoca repozytorium. Jezeli interface jak np. UserRepository rozszerza interface
        //JpaRepository to mozemy skorzystac z gotowych metod JPA (takich jak np. metoda save = zapisanie do bazy danych).

        //Zwroc zapisanego uzytkownika

        return null;
    }

    private Set<UserRole> saveUserRoles(User user){
        Set<UserRole> userRoles = new HashSet<>();
        userRoles.add(new UserRole(user, roleRepository.findByName("USER")));

        userRoles.stream().forEach(userRole -> roleRepository.saveAndFlush(userRole.getRole()));

        return userRoles;
    }

    public boolean checkUserExists(String username, String email){
        return checkUsernameExists(username) || checkEmailExists(username);
    }

    public boolean checkUsernameExists(String username) {
        return null != findByUsername(username);

    }
    
    public boolean checkEmailExists(String email) {
        return null != findByEmail(email);

    }

    public void saveUser (User user) {
        userRepository.save(user);
    }
    
    public List<User> findUserList() {
        return userRepository.findAll();
    }
}
