package com.inzynierka.core.model;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class PrimaryAccount {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private int accountNumber;

    private BigDecimal accountBalance;

    @OneToMany(mappedBy = "primaryAccount", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<PrimaryTransaction> primaryTransactionList;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(int accountNumber) {
        this.accountNumber = accountNumber;
    }

    public BigDecimal getAccountBalance() {
        return accountBalance;
    }

    public void setAccountBalance(BigDecimal accountBalance) {
        this.accountBalance = accountBalance;
    }

    public List<PrimaryTransaction> getPrimaryTransactionList() {
        return primaryTransactionList;
    }

    public void setPrimaryTransactionList(List<PrimaryTransaction> primaryTransactionList) {
        this.primaryTransactionList = primaryTransactionList;
    }


    public static final class PrimaryAccountBuilder {
        private Long id;
        private int accountNumber;
        private BigDecimal accountBalance;
        private List<PrimaryTransaction> primaryTransactionList;

        public PrimaryAccountBuilder() {
        }

        public static PrimaryAccountBuilder aPrimaryAccount() {
            return new PrimaryAccountBuilder();
        }

        public PrimaryAccountBuilder withId(Long id) {
            this.id = id;
            return this;
        }

        public PrimaryAccountBuilder withAccountNumber(int accountNumber) {
            this.accountNumber = accountNumber;
            return this;
        }

        public PrimaryAccountBuilder withAccountBalance(BigDecimal accountBalance) {
            this.accountBalance = accountBalance;
            return this;
        }

        public PrimaryAccountBuilder withPrimaryTransactionList(List<PrimaryTransaction> primaryTransactionList) {
            this.primaryTransactionList = primaryTransactionList;
            return this;
        }

        public PrimaryAccount build() {
            PrimaryAccount primaryAccount = new PrimaryAccount();
            primaryAccount.setId(id);
            primaryAccount.setAccountNumber(accountNumber);
            primaryAccount.setAccountBalance(accountBalance);
            primaryAccount.setPrimaryTransactionList(primaryTransactionList);
            return primaryAccount;
        }
    }
}



