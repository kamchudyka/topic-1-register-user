package com.inzynierka.controller;

import java.security.Principal;

import com.inzynierka.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.inzynierka.repository.RoleRepository;
import com.inzynierka.core.model.PrimaryAccount;
import com.inzynierka.core.model.SavingsAccount;
import com.inzynierka.core.model.User;
import org.springframework.web.bind.annotation.ResponseStatus;

@Controller
public class HomeController {
	
	@Autowired
	private UserService userService;
	
	@Autowired
    private RoleRepository roleRepository;
	
	@RequestMapping("/")
	public String home() {
		return "redirect:/index";
	}
	
	@RequestMapping("/index")
    public String index() {
        return "index";
    }
	
	@RequestMapping(value = "/signup", method = RequestMethod.GET)
    public String signup(Model model) {
        User user = new User();

        model.addAttribute("user", user);

        return "signup";
    }
    //Ustaw mapowanie na adres "/signup" oraz metode POST
    public String signupPost(@ModelAttribute("user") User user, Model model) {

	    //sprawdz czy dany uzytkownik juz istnieje za pomoca wywolania metody checkUserExists z serwisu userService

        //jezeli tak to:

            //sprawdz czy istnieje o podanym adresie email za pomoca metody checkEmailExists z serwisu userService
            //jezeli tak to:
                //dodaj do modelu zmienna emailExists z wartoscia true aby pojawil sie komunikat bledu na stronie

            //sprawdz czy uzytkownik o podanym username juz istnieje za pomoca metody checkUsernameExists z serwisu userService
            //jezeli tak to:
                //dodaj do modelu zmienna usernameExists z wartoscia true aby pojawil sie komunikat bledu na stronie

        //zwroc widok signup

        //jezeli nie to:
            //wywolaj wczesniej zaimplementowana metode createUser z serwisu userService podajac jako argument obiekt User ktory przyszedl wraz z zadaniem
            //przekieruj uzytkownika na strone index

	    return null;
    }

	@RequestMapping("/mainPage")
	public String mainPage(Principal principal, Model model) {
        User user = userService.findByUsername(principal.getName());
        PrimaryAccount primaryAccount = user.getPrimaryAccount();
        SavingsAccount savingsAccount = user.getSavingsAccount();

        model.addAttribute("primaryAccount", primaryAccount);
        model.addAttribute("savingsAccount", savingsAccount);

        return "mainPage";
    }
}
